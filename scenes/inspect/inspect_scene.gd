extends Control

signal back

const base_egg_path := "res://assets/graphics/eggs/egg_detail_stage_"

const combined_stats_scene := preload("res://scenes/displays/combined_stats.tscn")
const anim_time := 0.5

func set_item(item: Observable):
	clear_stats()
	if item is EggStats:
		var egg := item as EggStats
		var stage := egg.stage
		var egg_path := base_egg_path + str(stage) + "_base.png"
		var egg_detail_path := base_egg_path + str(stage) + "_detail.png"
		$InspectedItem/Item.texture = load(egg_path)
		$InspectedItem/Item.self_modulate = egg.base_color
		$InspectedItem/Details.texture = load(egg_detail_path)
		$InspectedItem/Details.visible = true		
		$InspectedItem/Details.self_modulate = egg.detail_color		
		$RaiseTween.interpolate_property($InspectedItem, "rect_position:x", 640, 280, anim_time, Tween.TRANS_BACK, Tween.EASE_OUT)
		set_up_egg_stats(egg)
	else:
		$InspectedItem.rect_position.x = 640
		$InspectedItem/Item.texture = item.detail_texture
		$InspectedItem/Item.self_modulate = Color(1,1,1)
		$InspectedItem/Details.visible = false
	$RaiseTween.interpolate_property($InspectedItem, "rect_position:y", 720, 100, anim_time, Tween.TRANS_BACK, Tween.EASE_OUT)
	$RaiseTween.start()

func clear_stats():
	for child in $InspectedStats/StatsContainer.get_children():
		$InspectedStats/StatsContainer.remove_child(child)
	$InspectedStats.rect_position.x = 1280

func set_up_egg_stats(egg: EggStats):
	# add summary
	var summary_stats := combined_stats_scene.instance() as CombinedStats
	summary_stats.update_from_egg(egg)
	$InspectedStats/StatsContainer.add_child(summary_stats)
	# add parts
	for part_label in egg.parts:
		var part := egg.parts[part_label] as Dictionary
		var grow_part = part["part"] as GrowPart
		var part_stats := combined_stats_scene.instance() as CombinedStats
		part_stats.update_from_egg_part(grow_part, part["level"], part["size"])
		print(grow_part)
		print(part["level"])
		$InspectedStats/StatsContainer.add_child(part_stats)
	# animate
	$RaiseTween.interpolate_property($InspectedStats, "rect_position:x", 1280, 640, anim_time + 0.2, Tween.TRANS_BACK, Tween.EASE_OUT)

func _on_BackButton_pressed():
	emit_signal("back")
