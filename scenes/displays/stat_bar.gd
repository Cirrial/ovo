extends Control

export var starting_icon: Texture

var value: int = 0

func _ready():
	if starting_icon != null:
		set_icon(starting_icon)

func set_icon(tex: Texture):
	$StatIcon.texture = tex

func set_tint(tint: Color):
	$StatProgress.tint_progress = tint

func set_value(new_value):
	value = new_value
	$StatProgress.value = new_value
