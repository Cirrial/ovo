extends Control

func _ready():		
	visible = false
	yield(get_tree(), "idle_frame")
	$Tweener.interpolate_property(self, "rect_scale:x", 0, 1, 0.5, Tween.TRANS_BACK, Tween.EASE_OUT)
	$Tweener.interpolate_property(self, "rect_scale:y", 0, 1, 0.5, Tween.TRANS_BACK, Tween.EASE_OUT)	
	$Tweener.start()
	visible = true

func update_from_growable(growable: Growable):		
	var icon := growable.grow_part.icon_texture
	$GrowableIcon.texture = icon
	if growable.level != $LevelProgress.value || growable.size != $SizeProgress.value:		
		$Tweener.interpolate_property($LevelProgress, "value", $LevelProgress.value, growable.level, 0.5, Tween.TRANS_BACK, Tween.EASE_OUT)
		$Tweener.interpolate_property($SizeProgress, "value", $SizeProgress.value, growable.size, 0.5, Tween.TRANS_BACK, Tween.EASE_OUT)
		$Tweener.start()
	
