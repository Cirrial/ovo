extends Control
class_name CombinedStats

const big_egg_icon = preload("res://assets/graphics/symbols/whole_egg_big.png")
const level_color = Color(0.541, 0.8, 0.752)
const size_color = Color(0.435, 0.592, 0.435)

func update_from_growable(growable: Growable):
	var color := growable.grow_part.color
	var big_icon := growable.grow_part.big_icon_texture
	$OrganIcon.texture = big_icon
	$LevelBar.set_tint(color)
	$LevelBar.set_value(growable.level)
	$SizeBar.set_tint(color)
	$SizeBar.set_value(growable.size)
	
func update_from_egg(egg: EggStats):
	$OrganIcon.texture = big_egg_icon
	$LevelBar.set_tint(level_color)
	$LevelBar.set_value(egg.total_level)
	$SizeBar.set_tint(size_color)
	$SizeBar.set_value(egg.total_size)
	
func update_from_egg_part(grow_part: GrowPart, level: int, size: int):
	var color := grow_part.color
	var big_icon := grow_part.big_icon_texture
	$OrganIcon.texture = big_icon
	$LevelBar.set_tint(color)
	$LevelBar.set_value(level)
	$SizeBar.set_tint(color)
	$SizeBar.set_value(size)

func update_as_total_overview(level: int, size: int):
	$OrganIcon.texture = big_egg_icon
	$LevelBar.set_tint(level_color)
	$LevelBar.set_value(level)
	$SizeBar.set_tint(size_color)
	$SizeBar.set_value(size)
