extends Label

var shadow_x: int = 0
var shadow_y: int = 0
const shadow_range: int = 5

func _ready():
	shift_shadow()
	
func shift_shadow():
	var old_shadow_x := shadow_x
	var old_shadow_y := shadow_y
	shadow_x = rand_range(-shadow_range, shadow_range) as int
	shadow_y = rand_range(-shadow_range, shadow_range) as int		
	$ShadowTween.interpolate_method(self, "set_shadow_x_offset", old_shadow_x, shadow_x, 1.5, Tween.TRANS_CIRC)
	$ShadowTween.interpolate_method(self, "set_shadow_y_offset", old_shadow_y, shadow_y, 1.5, Tween.TRANS_CIRC)
	$ShadowTween.start()

func set_shadow_x_offset(value):
	add_constant_override("shadow_offset_x", value)
	
func set_shadow_y_offset(value):
	add_constant_override("shadow_offset_y", value)

func _on_ShadowTween_tween_all_completed():
	shift_shadow()
	
func _on_FunkyLabel_visibility_changed():
	if visible:		
		$ShadowTween.remove_all()
		shift_shadow()
