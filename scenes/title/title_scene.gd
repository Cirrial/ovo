extends Control
signal start

func _ready():
	$ScoreLabel.visible = false

func get_score(score: int):
	$ScoreLabel.text = "Your score: " + str(score)
	$ScoreLabel.visible = true

func _on_StartButton_pressed():
	emit_signal("start")


func _on_StartButton_mouse_entered():
	pass # Replace with function body.
