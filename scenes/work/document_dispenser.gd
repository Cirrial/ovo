extends Sprite
signal document_selected(document)
signal on_hover

var document_definitions = []
var documents = {}
var documents_completed = {}
var document_buttons = {}

var document_button_scene := preload("res://scenes/buttons/document_button.tscn")

func receive_definitions(definitions):
	document_definitions = definitions

func reset():
	clear_all_documents()
	add_documents_to_container()

func clear_all_documents():
	documents = {}
	documents_completed = {}
	for document in document_buttons:
		var button = document_buttons[document]
		button.queue_free()
	document_buttons = {}

func add_documents_to_container():		
	var completed_doc_labels := get_completed_document_list()
	for document_def in document_definitions:
		if document_def in documents_completed or document_def in document_buttons:
			continue
		if document_def is ClientOrder:
			var order_def := document_def as ClientOrder
			if not order_def.are_prereqs_met(completed_doc_labels):
				continue
		# we're clear to add this document		
		add_document(document_def)

func add_document(document: Document):
	var button := document_button_scene.instance()
	button.set_document(document)
	button.connect("pressed", self, "_on_document_press", [document])	
	$DocumentContainer.add_child(button)
	document_buttons[document] = button

func remove_document(document: Document):
	if document in document_buttons:		
		var button = document_buttons[document]
		document_buttons.erase(document)
		button.visible = false
		yield(button, "press_sound_finished")
		button.queue_free()

func complete_document(document: Document):
	remove_document(document)
	documents_completed[document] = true
	if len(documents_completed) == len(document_definitions):
		show_completion()
	else:
		# add any new documents that we can
		add_documents_to_container()

func show_completion():
	# play a sound and shake to indicate nothing else is coming
	$EmptySound.play()
	$ShakePlayer.play("shake")

func get_completed_document_list() -> Array:
	var label_list := []
	for document in documents_completed:
		label_list.append(document.label)
	return label_list

func hide_all_documents():
	for button in $DocumentContainer.get_children():
		button.disabled = true
		button.visible = false

func show_all_documents():
	for button in $DocumentContainer.get_children():
		button.disabled = false
		button.visible = true

func show_all_but_specific_document(document: Document):
	show_all_documents()
	if document in document_buttons:
		var button := document_buttons[document] as BaseButton
		button.disabled = true
		button.visible = false

func _on_document_press(document: Document):	
	emit_signal("document_selected", document)

func _on_HoverArea_mouse_entered():
	emit_signal("on_hover")
