extends Sprite
signal egg_selected(egg)

var egg_button := preload("res://scenes/buttons/egg_icon_button.tscn")

var eggs := []
var egg_buttons := {}

func reset():
	clear_all_eggs()

func clear_all_eggs():
	for egg in eggs:
		var button = egg_buttons[egg]
		button.queue_free()
		egg_buttons.erase(egg)
	eggs = []

func add_egg(egg: EggStats):
	eggs.append(egg)
	var button := egg_button.instance()
	button.show_egg(egg)
	button.connect("pressed", self, "_on_egg_pressed", [egg])
	$EggContainer.add_child(button)
	egg_buttons[egg] = button

func remove_egg(egg: EggStats):
	if egg in eggs:
		eggs.remove(eggs.find(egg))
		var button = egg_buttons[egg]
		egg_buttons.erase(egg)	
		button.visible = false
		yield(button, "press_sound_finished")
		button.queue_free()		

func show_all_eggs():
	for button in $EggContainer.get_children():
		button.disabled = false
		button.visible = true

func show_all_but_one_egg(egg: EggStats):
	show_all_eggs()
	if egg in egg_buttons:
		var button := egg_buttons[egg] as BaseButton
		button.disabled = true
		button.visible = false

func _on_egg_pressed(egg: EggStats):
	emit_signal("egg_selected", egg)
