extends Sprite
signal add_item
signal took_item(item)
signal return_item(item)
signal submit(order, egg)
signal on_hover

var submit_chute_egg: EggStats
var submit_chute_order: ClientOrder

func _ready():
	$CurrentChuteEgg.visible = false
	$CurrentChuteOrder.visible = false	

func add_item(item):
	if item is ClientOrder:
		if submit_chute_order != null:
			# put existing order back
			emit_signal("return_item", submit_chute_order)
		var order := item as ClientOrder
		submit_chute_order = order
		$CurrentChuteOrder.texture = order.preview
		$CurrentChuteOrder.visible = true
		$ClearChuteButton.disabled = false
		if submit_chute_egg != null:
			$SubmitChuteButton.disabled = false
		emit_signal("took_item", item)
	elif item is EggStats:
		if submit_chute_egg != null:
			# put existing egg back
			emit_signal("return_item", submit_chute_egg)
		var egg := item as EggStats
		submit_chute_egg = egg
		$CurrentChuteEgg.show_egg(egg)
		$CurrentChuteEgg.visible = true
		$ClearChuteButton.disabled = false
		if submit_chute_order != null:
			$SubmitChuteButton.disabled = false
		emit_signal("took_item", item)

func clear_items():
	if submit_chute_egg != null:
		emit_signal("return_item", submit_chute_egg)
		submit_chute_egg = null
		$CurrentChuteEgg.visible = false
	if submit_chute_order != null:
		emit_signal("return_item", submit_chute_order)		
		submit_chute_order = null
		$CurrentChuteOrder.visible = false
	$ClearChuteButton.disabled = true
	$SubmitChuteButton.disabled = true
	
func submit_items():
	# get score of egg for given order
	var score := submit_chute_order.score_egg(submit_chute_egg)
	$CurrentChuteEgg.visible = false
	$CurrentChuteOrder.visible = false
	emit_signal("submit", submit_chute_order, submit_chute_egg)
	submit_chute_egg = null
	submit_chute_order = null
	$ClearChuteButton.disabled = true
	$SubmitChuteButton.disabled = true


func _on_SubmitChuteButton_pressed():
	submit_items()

func _on_AddChuteButton_pressed():
	emit_signal("add_item")

func _on_ClearChuteButton_pressed():
	clear_items()

func _on_HoverArea_mouse_entered():
	emit_signal("on_hover")
