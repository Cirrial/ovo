extends TextureButton
signal on_hover

func _on_ExitDoor_mouse_entered():
	emit_signal("on_hover")
	if !disabled:
		$HoverSound.play()
	
