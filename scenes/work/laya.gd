extends Sprite
class_name Laya

const face_left := preload("res://assets/graphics/character/laya-left.png")
const face_right := preload("res://assets/graphics/character/laya-right.png")
const face_left_down := preload("res://assets/graphics/character/laya-left-down.png")
const face_right_down := preload("res://assets/graphics/character/laya-right-down.png")
const face_inward := preload("res://assets/graphics/character/laya-inward.png")

enum FACING {
	FACE_LEFT,
	FACE_RIGHT,
	FACE_LEFT_DOWN,
	FACE_RIGHT_DOWN,
	FACE_INWARD
}

var facing_mapping := {
	FACING.FACE_LEFT: face_left,
	FACING.FACE_RIGHT: face_right,
	FACING.FACE_LEFT_DOWN: face_left_down,
	FACING.FACE_RIGHT_DOWN: face_right_down,
	FACING.FACE_INWARD: face_inward
}

func face_dir(facing):
	var new_texture: Texture
	if facing in facing_mapping:
		new_texture = facing_mapping[facing]
	else:
		new_texture = face_left_down
	texture = new_texture
