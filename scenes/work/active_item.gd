extends Control
signal put_item_back(item)
signal examine_item(item)

var active_item

func _ready():
	$DocumentButton.disabled = true
	$DocumentButton.visible = false	
	$EggButton.disabled = true
	$EggButton.visible = false

func set_active_item(item):
	if active_item != null:
		emit_signal("put_item_back", active_item)
	active_item = item
	if item is Document:
		var doc := item as Document
		$DocumentButton.show_document(doc)
		$DocumentButton.disabled = false
		$DocumentButton.visible = true		
		$EggButton.disabled = true
		$EggButton.visible = false
	elif item is EggStats:
		var egg := item as EggStats
		$EggButton.show_egg(egg)
		$EggButton.disabled = false
		$EggButton.visible = true		
		$DocumentButton.disabled = true
		$DocumentButton.visible = false

func clear():
	if active_item != null:
		emit_signal("put_item_back", active_item)
	active_item = null
	$DocumentButton.disabled = true
	$DocumentButton.visible = false	
	$EggButton.disabled = true
	$EggButton.visible = false

func clear_without_return():
	active_item = null
	$DocumentButton.disabled = true
	$DocumentButton.visible = false	
	$EggButton.disabled = true
	$EggButton.visible = false

func _on_item_button_pressed():
	emit_signal("examine_item", active_item)
