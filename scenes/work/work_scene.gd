extends Control
signal start_egg(order)
signal observe_item(item)
signal score(points)
signal leave_room()

func _ready():
	var loader := Loader.new()
	var order_defs := loader.load_defs("res://resources/orders")
	$OrderDispenser.receive_definitions(order_defs)
	var doc_defs := loader.load_defs("res://resources/docs")
	$HelpDispenser.receive_definitions(doc_defs)
	reset()

func reset():
	$ActiveItem.clear()
	$EggContainer.reset()	
	$OrderDispenser.reset()
	$HelpDispenser.reset()

func add_egg(egg: EggStats):
	$EggContainer.add_egg(egg)

func put_item_back(item):
	if item is ClientOrder:
		$OrderDispenser.add_document(item)
	elif item is Document:
		$HelpDispenser.add_document(item)
	else:
		$EggContainer.add_egg(item)

func _on_document_selected(document: Document):
	$ActiveItem.set_active_item(document)
	if document is ClientOrder:
		$OrderDispenser.remove_document(document)
	else:
		$HelpDispenser.remove_document(document)

func _on_egg_selected(egg: EggStats):
	$ActiveItem.set_active_item(egg)	
	$EggContainer.remove_egg(egg)	

# BUTTON HANDLERS HERE ON OUT

func _on_EggButton_pressed():
	var active_order = null
	if $ActiveItem.active_item is ClientOrder:
		active_order = $ActiveItem.active_item as ClientOrder
	emit_signal("start_egg", active_order)

func _on_ExitDoor_pressed():
	emit_signal("leave_room")

func _on_SubmissionChute_submit(order: ClientOrder, egg: EggStats):
	# remove the items involved
	$OrderDispenser.complete_document(order)
	$EggContainer.remove_egg(egg)
	# calculate score	
	var score := order.score_egg(egg)
	emit_signal("score", score)

func _on_SubmissionChute_return_item(item):
	put_item_back(item)

func _on_SubmissionChute_add_item():
	# pass active item to chute
	$SubmissionChute.add_item($ActiveItem.active_item)

func _on_SubmissionChute_took_item(item):
	# item was accepted, remove it from the relevant places
	if item is ClientOrder:
		$OrderDispenser.remove_document(item)
		$ActiveItem.clear_without_return()
	elif item is EggStats:
		$EggContainer.remove_egg(item)
		$ActiveItem.clear_without_return()

func _on_ActiveItem_examine_item(item):
	emit_signal("observe_item", item)

func _on_ActiveItem_put_item_back(item):
	put_item_back(item)

# HOVER FUNCTIONS (MOVE LAYA HEAD)

func _on_SubmissionChute_on_hover():
	$Laya.face_dir($Laya.FACING.FACE_RIGHT)

func _on_OrderDispenser_on_hover():
	$Laya.face_dir($Laya.FACING.FACE_RIGHT_DOWN)

func _on_HelpDispenser_on_hover():
	$Laya.face_dir($Laya.FACING.FACE_LEFT_DOWN)

func _on_MiniEggButton_on_hover():
	$Laya.face_dir($Laya.FACING.FACE_INWARD)

func _on_ExitDoor_on_hover():
	$Laya.face_dir($Laya.FACING.FACE_LEFT)
