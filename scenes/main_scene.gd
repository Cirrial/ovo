extends Control

var grow_scene: Node
var work_scene: Node
var inspect_scene: Node
var title_scene: Node

var bg_music := [
	preload("res://assets/music/fonkdongo_plomo.ogg"),
	preload("res://assets/music/funked_up.ogg"),
	preload("res://assets/music/high_funktoining_autism.ogg"),
	preload("res://assets/music/honey_bear_loop.ogg")
]

var ambience := [
	preload("res://assets/sounds/ambience/bubbling_ambience.ogg"),
	preload("res://assets/sounds/ambience/slimy_ambience.ogg")
]

export var sound_muted: bool = false
export var music_muted: bool = false

var score: int = 0

func _ready():
	if sound_muted:
		set_sound_muted(true)
	if music_muted:
		set_music_muted(true)
	grow_scene = $Scenes/GrowScene
	work_scene = $Scenes/WorkScene
	inspect_scene = $Scenes/InspectScene
	title_scene = $Scenes/TitleScene
	$Scenes.remove_child(work_scene)
	$Scenes.remove_child(grow_scene)
	$Scenes.remove_child(inspect_scene)
	play_random_ambience()
	
func play_random(player: AudioStreamPlayer, choices: Array) -> void:
	var sound := choices[randi() % len(choices)] as AudioStream	
	player.stream = sound	
	player.play()
	player.stream_paused = false

func play_random_ambience():
	play_random($AmbiencePlayer, ambience)

func play_random_music():
	play_random($MusicPlayer, bg_music)

func transition(from, to):	
	$Scenes.remove_child(from)
	$Scenes.add_child(to)

func _on_WorkScene_start_egg(order: ClientOrder):
	$TransitionSound.play()
	transition(work_scene, grow_scene)
	grow_scene.receive_order(order)

func _on_GrowScene_finish_egg(egg: EggStats):	
	$EggSound.play()
	transition(grow_scene, work_scene)	
	work_scene.add_egg(egg)

func _on_WorkScene_score(points):
	score += points
	print("Points: " + str(score))

func _on_WorkScene_observe_item(item):
	$TransitionSound.play()		
	transition(work_scene, inspect_scene)
	inspect_scene.set_item(item)

func _on_WorkScene_leave_room():
	$BackSound.play()
	$MusicPlayer.stop()
	$MusicPlayer.stream_paused = true
	play_random_ambience()
	transition(work_scene, title_scene)
	title_scene.get_score(score)

func _on_InspectScene_back():
	$BackSound.play()
	transition(inspect_scene, work_scene)

func _on_GrowScene_back():
	$BackSound.play()
	transition(grow_scene, work_scene)

func _on_TitleScene_start():
	$TransitionSound.play()
	$AmbiencePlayer.stop()
	$AmbiencePlayer.stream_paused = true	
	play_random_music()	
	score = 0
	work_scene.reset()
	transition(title_scene, work_scene)

func _on_MusicPlayer_finished():
	play_random_music()

func _on_AmbiencePlayer_finished():
	play_random_ambience()

func _on_SoundToggleButton_pressed():
	set_sound_muted(!sound_muted)

func _on_MusicToggleButton_pressed():
	set_music_muted(!music_muted)

func set_sound_muted(value: bool):
	sound_muted = value
	AudioServer.set_bus_mute(AudioServer.get_bus_index("Sound"), value)
	$SoundToggleButton.muted = value

func set_music_muted(value: bool):
	music_muted = value
	AudioServer.set_bus_mute(AudioServer.get_bus_index("Music"), value)
	$MusicToggleButton.muted = value
