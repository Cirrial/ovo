extends TextureButton

var label: String

func init(grow_part: GrowPart):
	label = grow_part.label
	texture_normal = grow_part.biotemplate_texture
	texture_pressed = grow_part.biotemplate_pressed_texture
	texture_disabled = grow_part.biotemplate_disabled_texture
	texture_hover = grow_part.biotemplate_hover_texture


func _on_BiotemplateButton_pressed():
	$PressSound.play()


func _on_BiotemplateButton_mouse_entered():
	if !disabled:
		$HoverSound.play()
