extends Control

var growable_stats = {} # key is growable, value is stats scene

var part_stat_scene = preload("res://scenes/displays/growable_dial.tscn")

func empty_stats():
	for child in $StatsContainer.get_children():
		$StatsContainer.remove_child(child)
	growable_stats = {}
	
func update_from_growables(growables):
	for growable_label in growables:
		var growable = growables[growable_label]
		var growable_stat_display
		if growable in growable_stats: 
			# if we have an existing stat display, update
			growable_stat_display = growable_stats[growable]
		else: 
			# otherwise, make a new one and add it
			growable_stat_display = part_stat_scene.instance()
			growable_stats[growable] = growable_stat_display
			$StatsContainer.add_child(growable_stat_display)			
		growable_stat_display.update_from_growable(growable)

func update_growable(growable):
	if growable in growable_stats:
		var growable_stat_display = growable_stats[growable]
		growable_stat_display.update_from_growable(growable)
