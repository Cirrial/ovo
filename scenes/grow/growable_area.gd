tool

extends Node2D

export(float, 10, 1000) var bounding_radius setget set_radius
export var show_circle: bool = false

func set_radius(new_radius):
	if new_radius != bounding_radius:
		bounding_radius = new_radius
		update()

func _draw():
	if show_circle:
		draw_circle(Vector2(0, 0), bounding_radius, Color(0, 0.5, 0.75, 0.25))

func empty():
	for child in get_children():
		child.queue_free()
