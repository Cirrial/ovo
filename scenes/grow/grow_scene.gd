extends Control
signal finish_egg(egg)
signal back

var biotemplate_button_scene := preload("res://scenes/grow/biotemplate_button.tscn")

var total_size: int = 0
var max_size: int = 10
export var skip_animations: bool = true

func _ready():
	var loader := Loader.new()
	var grow_defs := loader.load_defs("res://resources/grow_parts")
	for grow_def in grow_defs:
		var button := biotemplate_button_scene.instance()		
		button.init(grow_def)
		button.connect("pressed", self, "_grow_button_pressed", [button, grow_def])		
		$GrowButtonContainer.add_child(button)

func receive_order(order: ClientOrder):
	if order != null:
		pass

func disable_all_buttons():
	for button in $GrowButtonContainer.get_children():
		button.disabled = true
	$EggButton.disabled = true
	$BackButton.disabled = true

func enable_all_buttons():
	for button in $GrowButtonContainer.get_children():
		if not button.label in $IncubationSac.growables:
			button.disabled = false	

func _grow_button_pressed(button: Button, grow_def: Resource):
	disable_all_buttons()
	if not grow_def.label in $IncubationSac.growables:		
		# add new growable to sac
		$IncubationSac.add_part(grow_def, skip_animations)		
		if !skip_animations:
			yield($IncubationSac, "add_part_finished")
		
		# send action signal for all growables
		var all_actions := []
		for growable_label in $IncubationSac.growables:
			var growable = $IncubationSac.growables[growable_label]
			var just_added := false
			if growable_label == grow_def.label:
				just_added = true
			var actions = growable.get_actions_for_round($IncubationSac.growables, just_added)
			all_actions += actions
		
		# apply all actions
		for action in all_actions:
			print(action)
			if !skip_animations:
				action.origin.flash(3)			
				yield(action.origin, "flash_finished")
				action.destination.apply_action(action)			
				yield(action.destination, "grow_finished")
			else:
				action.destination.apply_action(action)
				action.destination.skip_anims()
			$SacStats.update_growable(action.destination)
	
	var new_total_size := $IncubationSac.calculate_total_size() as int
	if new_total_size != total_size:
		total_size = new_total_size
		print(total_size)
		$CapacityDial.update_size_display(total_size)
	
	# if the size is at max, do not enable adding any more parts
	if total_size < max_size:
		enable_all_buttons()
	if total_size > 0: # no empty eggs
		$EggButton.disabled = false
	
	var total_level := $IncubationSac.calculate_total_level() as int
	$EggLevelDial.update_level_display(total_level)
	$SacStats.update_from_growables($IncubationSac.growables)

func clear_all():	
	total_size = 0
	$IncubationSac.clear()		
	$CapacityDial.reset()
	$SacStats.empty_stats()
	enable_all_buttons()
	$EggButton.disabled = true
	$BackButton.disabled = false

func _on_EggButton_pressed():	
	# TODO: some sort of outro animation, make all the growables shrink and an egg graphic pop up?
	var egg := EggStats.new()
	egg.give_growables($IncubationSac.growables)
	clear_all()	
	emit_signal("finish_egg", egg)

func _on_BackButton_pressed():
	emit_signal("back")
