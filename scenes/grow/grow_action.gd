extends Reference
class_name GrowAction

var origin
var destination
var size_change: int
var level_change: int

func _init(origin, destination, size_change, level_change) -> void:
	self.origin = origin
	self.destination = destination
	self.size_change = size_change
	self.level_change = level_change

func _to_string() -> String:
	return "(" + self.origin.label + " -> " + self.destination.label + ")" + " sz:" + str(size_change) + " lv:" + str(level_change)
