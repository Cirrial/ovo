extends TextureProgress

func reset():
	value = 0

func update_size_display(size):
	if value != size:
		$CapacityTween.interpolate_property(self, "value", value, size, 0.5, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
		$CapacityTween.start()	
		$CapacityChangeSound.pitch_scale = 0.5 + min(0.5, size * 0.05)
		$CapacityChangeSound.play()
		if size >= 10:
			$CapacityMaxSound.play()
