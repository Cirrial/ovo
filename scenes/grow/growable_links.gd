extends Node2D

var linking_curves := {} # key is "origin-destination"
# value is a dictionary of a curve2d, a line2d, a reference to an origin node2d, and a destination node2d

func add_link(origin: Growable, destination: Growable):
	var link := {}
	link["origin"] = origin
	link["destination"] = destination
	var curve := Curve2D.new()
	curve.add_point(origin.position)
	curve.add_point(destination.position)
	link["curve"] = curve
	var line := Line2D.new()
	line.default_color = Color(1, 1, 1, 1)
	line.end_cap_mode = Line2D.LINE_CAP_ROUND
	line.joint_mode = Line2D.LINE_JOINT_ROUND
	line.width = 5
	add_child(line)
	link["line"] = line
	link["control_a"] = Vector2(0, 0)
	link["control_b"] = Vector2(0, 0)
	linking_curves[origin.label + "-" + destination.label] = link

func _process(delta):
	for label in linking_curves:
		var linking_curve = linking_curves[label]
		var curve = linking_curve["curve"]
		var line = linking_curve["line"]
		var origin = linking_curve["origin"]
		var destination = linking_curve["destination"]
		# update curve
		curve.clear_points()
		curve.add_point(origin.global_position)
		curve.add_point(destination.global_position)
		# update line from curve
		line.clear_points()
		for point in curve.get_baked_points():
			line.add_point(point)

func empty():
	for child in get_children():
		child.queue_free()
	linking_curves = {}
