extends TextureProgress

func reset():
	value = 0

func update_level_display(level):
	if level != value:
		$LevelTween.interpolate_property(self, "value", value, level, 0.5, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
		$LevelTween.start()	
		# $LevelChangeSound.pitch_scale = 0.5 + min(0.5, size * 0.025)
		# $LevelChangeSound.play()
