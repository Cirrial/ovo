extends RigidBody2D
class_name Growable

signal flash_finished
signal grow_finished

var grow_part: GrowPart
var bounding_area_radius: float = 0

var label: String
var level: int
var size: int # this is logical size, not sprite size or physics size
var rounds: int

var twitch_impulse: int = 20
var twitch_time_min: float = 0.2
var twitch_time_max: float = 3.0
var radius: float

var target_icon_scale: float = 1
var growth_icon_scale: float = 1

func _ready():
	# avoid sharing the same circle shape for all growables
	radius = $BoundingCircle.shape.radius
	var unique_circle_shape := CircleShape2D.new()
	unique_circle_shape.radius = 0
	$BoundingCircle.shape = unique_circle_shape
	$Icon.scale = Vector2(0,0)
	# random rotation
	rotation = rand_range(0, PI*2)
	do_grow(true)

func init(grow_part: GrowPart, bounding_area_radius: float):
	self.grow_part = grow_part
	self.bounding_area_radius = bounding_area_radius
	label = grow_part.label
	level = 1
	rounds = 0
	size = grow_part.initial_size 
	$Icon.texture = grow_part.organ_texture
	
func _physics_process(delta):
	# check if we're about to leave the bounding circle
	var radius: float = $BoundingCircle.shape.radius
	var distance := position.length()
	if distance + radius > bounding_area_radius:
		var impulse := -position
		impulse = impulse.normalized() * twitch_impulse
		apply_impulse(Vector2.ZERO, impulse)
	

func get_actions_for_round(growables: Dictionary, just_added: bool) -> Array:
	# keep track of what actions will happen as a result of this growable
	var actions: Array = []
	
	if just_added:		
		# apply changes to all others on add
		if grow_part.size_increase_all_others_on_add != 0:
			for growable_label in growables:
				if growable_label == label:
					continue
				var growable = growables[growable_label]
				var action = GrowAction.new(self, growable, grow_part.size_increase_all_others_on_add, grow_part.level_increase_all_others_on_add)
				actions.append(action)
	else:					
		# TODO: THIS
		if rounds >= grow_part.activation_delay:
			# first off, self changes (if not just added): 
			if grow_part.size_increase_per_round != 0:
				var action = GrowAction.new(self, self, grow_part.size_increase_per_round, grow_part.level_increase_per_round)
				actions.append(action)
			# then, changing others (if not just added):
			if grow_part.size_increase_others_per_round.size() > 0:
				for label in grow_part.size_increase_others_per_round:
					if label in growables:
						var other_growable = growables[label]
						var change = grow_part.size_increase_others_per_round[label]
						var action = GrowAction.new(self, other_growable, change, 1)
						actions.append(action)
						
	rounds += 1
		
	return actions

func do_grow(get_bigger: bool):
	$PulseTween.stop($Icon)	
	var factor := pow(1.2, size)
	growth_icon_scale = factor
	target_icon_scale = growth_icon_scale	
	$GrowTween.interpolate_property($Icon, "scale:x", $Icon.scale.x, target_icon_scale, 1.0, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	$GrowTween.interpolate_property($Icon, "scale:y", $Icon.scale.y, target_icon_scale, 1.0, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	$GrowTween.interpolate_property($BoundingCircle, "shape:radius", $BoundingCircle.shape.radius, radius * factor, 1.0, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	$GrowTween.interpolate_property($HitSound, "pitch_scale", $HitSound.pitch_scale, max(0.1, 2 - target_icon_scale), 1.0, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	$GrowTween.start()	
	if get_bigger:
		$GrowSound.play()
	else:
		$ShrinkSound.play()

func apply_action(action: GrowAction):
	if action.destination != self:
		print("got wrong action, somehow, whoops")
		return
	level += action.level_change
	size += action.size_change
	var bigger := action.size_change >= 0
	do_grow(bigger)
	if action.origin != self:
		# also do a flash for visibility
		flash(0.6)

func _on_TwitchTimer_timeout():	
	var impulse := Vector2(rand_range(-1, 1), rand_range(-1, 1))
	impulse = impulse.normalized() * twitch_impulse
	apply_impulse(Vector2.ZERO, impulse)
	$TwitchTimer.start(rand_range(twitch_time_min, twitch_time_max))

func _on_PulseTimer_timeout():
	if target_icon_scale <= growth_icon_scale:
		target_icon_scale = growth_icon_scale * 1.1
	else:
		target_icon_scale = growth_icon_scale
	pulse()

func pulse():	
	if not $GrowTween.is_active():
		$PulseTween.interpolate_property($Icon, "scale:x", $Icon.scale.x, target_icon_scale, $PulseTimer.wait_time - 0.2, Tween.TRANS_BACK, Tween.EASE_OUT)
		$PulseTween.interpolate_property($Icon, "scale:y", $Icon.scale.y, target_icon_scale, $PulseTimer.wait_time - 0.2, Tween.TRANS_BACK, Tween.EASE_OUT)
		$ThrobSound.pitch_scale = max(2 - target_icon_scale, 0.1)
		$ThrobSound.play()
		$PulseTween.start()

func flash(strength: float):
	var time := 0.3
	$Icon.modulate = Color(strength, strength, strength)
	$FlashTween.interpolate_property($Icon, "modulate:r", strength, 1, time, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$FlashTween.interpolate_property($Icon, "modulate:g", strength, 1, time, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$FlashTween.interpolate_property($Icon, "modulate:b", strength, 1, time, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$FlashTween.start()
	$ActivateSound.play()

func skip_anims():
	$PulseTween.stop($Icon)
	$FlashTween.stop($Icon)
	$GrowTween.stop($Icon)
	$GrowTween.stop($BoundingCircle)
	var factor := pow(1.2, size)
	growth_icon_scale = factor
	target_icon_scale = growth_icon_scale
	$Icon.scale = Vector2(target_icon_scale, target_icon_scale)
	$BoundingCircle.shape.radius = radius * factor
	$HitSound.pitch_scale = max(2 - target_icon_scale, 0.1)

func _on_FlashTween_tween_all_completed():
	emit_signal("flash_finished")

func _on_GrowTween_tween_all_completed():
	emit_signal("grow_finished")

func _on_Growable_body_entered(body):
	if !$HitSound.playing:
		$HitSound.play()
