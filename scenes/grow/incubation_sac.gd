extends Control
signal add_part_finished

var growables := {} # key is label, value is node
var growable_scene := preload("res://scenes/grow/growable.tscn")

func add_part(grow_def: GrowPart, skip_animations: bool):
	var growable := growable_scene.instance()
	growable.init(grow_def, $GrowableArea.bounding_radius)
	$GrowableArea.add_child(growable)

	# set up links to this new growable
	for growable_label in growables:
		var other_growable = growables[growable_label]
		$LinkContainer.add_link(growable, other_growable)
	if !skip_animations:
		yield(growable, "grow_finished")
	else:
		growable.skip_anims()
	growables[grow_def.label] = growable
	emit_signal("add_part_finished")

func clear():
	$GrowableArea.empty()
	$LinkContainer.empty()
	growables = {}
	
func calculate_total_size() -> int:
	var new_total_size: int = 0
	for growable_label in growables:
		var growable = growables[growable_label]
		new_total_size += growable.size
	return new_total_size

func calculate_total_level() -> int:
	var new_total_level: int = 0
	for growable_label in growables:
		var growable = growables[growable_label]
		new_total_level += growable.level
	return new_total_level
