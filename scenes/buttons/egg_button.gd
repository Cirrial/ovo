extends TextureButton

export var play_sound_on_pressed: bool = true

func _on_EggButton_pressed():
	if play_sound_on_pressed:
		$PressSound.play()

func _on_EggButton_mouse_entered():
	if !disabled:
		$HoverSound.play()
