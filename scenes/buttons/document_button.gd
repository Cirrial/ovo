extends TextureButton
signal press_sound_finished

func set_document(document: Document):
	texture_normal = document.side_view
	texture_hover = document.side_view_hover
	texture_pressed = document.side_view_pressed

func _on_DocumentButton_mouse_entered():
	if !disabled:
		$HoverSound.play()

func _on_DocumentButton_pressed():
	$PressSound.play()

func _on_PressSound_finished():
	emit_signal("press_sound_finished")
