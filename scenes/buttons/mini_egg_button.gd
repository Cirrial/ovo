extends TextureButton
signal on_hover

func _on_MiniEggButton_pressed():
	$PressSound.play()

func _on_MiniEggButton_mouse_entered():
	emit_signal("on_hover")
	if !disabled:
		$HoverSound.play()
	
