extends TextureButton

export var normal_texture: Texture
export var normal_hover_texture: Texture
export var normal_pressed_texture: Texture

export var muted_texture: Texture
export var muted_hover_texture: Texture
export var muted_pressed_texture: Texture

export var muted: bool = false setget set_muted

func set_muted(value):
	muted = value
	if muted:
		texture_normal = muted_texture
		texture_hover = muted_hover_texture
		texture_pressed = muted_pressed_texture
	else:
		texture_normal = normal_texture
		texture_hover = normal_hover_texture
		texture_pressed = normal_pressed_texture

func _ready():
	texture_normal = normal_texture
	texture_hover = normal_hover_texture
	texture_pressed = normal_pressed_texture

func _on_AudioToggleButton_pressed():
	$PressSound.play()

func _on_AudioToggleButton_mouse_entered():
	if !disabled:
		$HoverSound.play()
