extends TextureButton
signal press_sound_finished

func show_egg(egg: EggStats):
	$EggIcon.show_egg(egg)

func _on_EggIconButton_mouse_entered():
	if !disabled:
		$HoverSound.play()

func _on_EggIconButton_pressed():
	$PressSound.play()

func _on_PressSound_finished():
	emit_signal("press_sound_finished")

