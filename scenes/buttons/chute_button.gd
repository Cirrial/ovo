extends TextureButton

export var press_sound: AudioStream = preload("res://assets/sounds/gloop-06.ogg")
export var press_volume: float = -3
export var press_pitch: float = 0.7

func _ready():
	$PressSound.stream = press_sound
	$PressSound.volume_db = press_volume
	$PressSound.pitch_scale = press_pitch

func _on_BasicButton_pressed():
	$PressSound.play()

func _on_BasicButton_mouse_entered():
	if !disabled:
		$HoverSound.play()
