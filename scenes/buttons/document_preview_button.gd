extends TextureButton

func show_document(document: Document):
	texture_normal = document.preview
	texture_hover = document.preview_hover
	texture_pressed = document.preview_pressed

func _on_DocumentButton_pressed():
	$PressSound.play()

func _on_DocumentButton_mouse_entered():
	if !disabled:
		$HoverSound.play()
