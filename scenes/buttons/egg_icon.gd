extends Control

var egg_stats: EggStats

var stage: int
var base_color: Color
var detail_color: Color

var base_normal: Texture
var base_hover: Texture
var base_pressed: Texture

var detail_normal: Texture
var detail_hover: Texture
var detail_pressed: Texture

const base_egg_path := "res://assets/graphics/eggs/"

func show_egg(egg: EggStats):
	stage = egg.stage
	base_color = egg.base_color
	detail_color = egg.detail_color
	var egg_path := base_egg_path + "egg_icon_stage_" + str(stage)
	base_normal = load(egg_path + "_base.png")
	base_hover = load(egg_path + "_base-hover.png")
	base_pressed = load(egg_path + "_base-pressed.png")
	detail_normal = load(egg_path + "_detail.png")
	detail_hover = load(egg_path + "_detail-hover.png")
	detail_pressed = load(egg_path + "_detail-pressed.png")
	$Base.self_modulate = base_color
	$Detail.self_modulate = detail_color
	set_normal()

func set_normal():
	$Base.texture = base_normal
	$Detail.texture = detail_normal

func set_hover():
	$Base.texture = base_hover
	$Detail.texture = detail_hover
	
func set_pressed():
	$Base.texture = base_pressed
	$Detail.texture = detail_pressed


func _on_EggButton_mouse_entered():
	set_hover()

func _on_EggButton_mouse_exited():
	set_normal()

func _on_EggButton_button_down():
	set_pressed()

func _on_EggButton_button_up():
	set_normal()
