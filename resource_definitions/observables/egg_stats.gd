extends Observable
class_name EggStats

const stage_map = {
	0: 0,
	1: 0,
	2: 0,
	3: 0,
	4: 1,
	5: 1,
	6: 1,
	7: 2,
	8: 2,
	9: 2,
	10: 2
}

export var parts: Dictionary = {}
var total_level: int = 0
var total_size: int = 0
var stage:int = 0
var base_color: Color
var detail_color: Color

func give_growables(growables: Dictionary):
	parts = {}
	label = ""
	var sorted_growables = sort_growables(growables)
	for growable_label in growables:
		var growable = growables[growable_label]
		parts[growable_label] = {
			"part": growable.grow_part,
			"level": growable.level,
			"size": growable.size
		}		
		total_level += growable.level
		total_size += growable.size
		label += growable.label.substr(0, 1)
	if total_level in stage_map:
		stage = stage_map[total_level]
	elif total_level < 0:
		stage = 0
	else:
		stage = 3
	base_color = sorted_growables[0].grow_part.color
	if len(sorted_growables) == 1:
		detail_color = sorted_growables[0].grow_part.color
	else:
		detail_color = sorted_growables[1].grow_part.color

func sort_growables(growables: Dictionary) -> Array:
	var growables_to_sort := []
	for growable_label in growables:
		var growable = growables[growable_label]
		growables_to_sort.append(growable)
	growables_to_sort.sort_custom(self, "sort_growables_level_desc")
	return growables_to_sort
	
func sort_growables_level_desc(a: Growable, b: Growable) -> bool:
	return a.level > b.level		

func _to_string() -> String:
	var out := "(" + label + ") size: " + str(total_size) + " lv: " + str(total_level)
	return out
