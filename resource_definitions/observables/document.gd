extends Observable
class_name Document

export var side_view: Texture = preload("res://assets/graphics/documents/side/side_tut_min_level.png")
export var side_view_hover: Texture = preload("res://assets/graphics/documents/side/side_tut_min_level-hover.png")
export var side_view_pressed: Texture = preload("res://assets/graphics/documents/side/side_tut_min_level-pressed.png")

export var preview: Texture = preload("res://assets/graphics/documents/preview/preview_tut_needs_part-hover.png")
export var preview_hover: Texture = preload("res://assets/graphics/documents/preview/preview_tut_needs_part-hover.png")
export var preview_pressed: Texture = preload("res://assets/graphics/documents/preview/preview_tut_needs_part-hover.png")

export var prerequisite_documents: Array = []

func _to_string() -> String:
	return label
