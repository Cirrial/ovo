extends Resource
class_name Observable

export var label: String
export var detail_texture: Texture = preload("res://assets/graphics/documents/detail/detail_explain_eat_for_egg.png")
