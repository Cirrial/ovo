extends Document
class_name ClientOrder

export var required_parts: Dictionary = {} # label is name of part, value is required minimum level for part
export var required_total_level: int = 0
export var required_min_size: int = 0
export var required_max_size:int = 0

func _to_string() -> String:
	var out := "(" + label + ") reqs: "
	if required_total_level > 0:
		out += "total lv " + str(required_total_level) + " "
	if required_min_size > 0:
		out += "min size " + str(required_min_size) + " "
	if required_max_size != 0:
		out += "max size " + str(required_max_size) + " "
	if len(required_parts) > 0:
		for required_part in required_parts:
			out += required_part + " (lv " + str(required_parts[required_part]) + ") "	
	return out

func score_egg(egg: EggStats) -> int:
	var score := 1 # give A point for at least submitting an egg
	var egg_meets_requirements = true
	# check if egg fits every criteria
	# check level
	if egg.total_level < required_total_level:
		egg_meets_requirements = false	
	# check size
	if required_max_size != 0 and egg.total_size > required_max_size:
		egg_meets_requirements = false
	if egg.total_size < required_min_size:
		egg_meets_requirements = false		
	# check parts
	for required_part in required_parts:
		if required_part in egg.parts:
			var required_level = required_parts[required_part]
			var part_level = egg.parts[required_part]["level"]
			if part_level < required_level:
				egg_meets_requirements = false
				break
		else:
			egg_meets_requirements = false
			break
	
	if egg_meets_requirements:
		score = egg.total_level # give full score if the egg meets standards
	return score
	
func are_prereqs_met(completed_order_labels: Array) -> bool:
	var all_prereqs_met := true
	for prereq in prerequisite_documents:
		if not prereq in completed_order_labels:
			all_prereqs_met = false
			break
	return all_prereqs_met 
