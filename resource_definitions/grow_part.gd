extends Resource
class_name GrowPart

export var label: String = "undefined"
export var initial_size: int = 1
export var minimum_size: int = 1
export var activation_delay: int = 0
export var size_increase_per_round: int = 0
export var level_increase_per_round: int = 1
export var size_increase_all_others_on_add: int = 0
export var level_increase_all_others_on_add: int = 0
export var size_increase_others_per_round: Dictionary = {}

export var organ_texture: Texture = preload("res://assets/graphics/organs/template_organ.png")
export var biotemplate_texture: Texture = preload("res://assets/graphics/biotemplates/template_biotemplate.png")
export var biotemplate_hover_texture: Texture = preload("res://assets/graphics/biotemplates/template_biotemplate-hover.png")
export var biotemplate_pressed_texture: Texture = preload("res://assets/graphics/biotemplates/template_biotemplate-pressed.png")
export var biotemplate_disabled_texture: Texture = preload("res://assets/graphics/biotemplates/template_biotemplate-disabled.png")
export var icon_texture: Texture = preload("res://assets/graphics/symbols/basic_grower.png")
export var big_icon_texture: Texture = preload("res://assets/graphics/symbols/basic_grower_big.png")

export var color: Color = Color(0.5, 0.5, 0.5)
