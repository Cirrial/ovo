extends Reference
class_name Loader

func load_defs(path: String) -> Array:
	var defs := []
	var dir := Directory.new()
	if dir.open(path) == OK:
		dir.list_dir_begin()
		var file_name := dir.get_next()
		while file_name != "":
			var name_parts := file_name.split(".")
			if name_parts[1] == "tres":
				var def := load(path + "/" + file_name)
				defs.append(def)
			file_name = dir.get_next()
	return defs

