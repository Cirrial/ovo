
v 0.2:

#8accc0 level 0.541, 0.8, 0.752
#6f976f size 0.435, 0.592, 0.435


ovo todo list:
x - sounds

x - break things up into sub-scenes so it's less of a higgledy-piggledy mess
	x - refactor the dispensers to have a common object and a unique texture in each instance
	x - make the submission chute its own scene
	x - set up egg container as its own scene
	x - set up current inventory item as its own scene
	x - set up capacity dial as its own scene
	x - set up incubation sac as its own scene

x - fix bug where restarting the game does not actually restart the game (orders remain consumed/submitted)
x - fix bug where adding to submit chute overwrites existing instead of replacing
x - fix bug where alien drawn over egg list

x - grow egg feedback
	- draw:
		x - size icon
		x - level icon
		x - progress notches (up to 30)
	x - code:
		x - scene for displaying a single fact:
			x - icon
			x - number of marks afterwards
		x - scene for displaying multiple facts

	redo stats display for organ parts: (keep what exists for detailed examination feedback) 
	 x - show level as a radial counter like for size (150 x 150) (0 - 20) (20 notches)
	 x - do it radially (100x100)
	 	x - icon in middle
	 	x - size around the icon (-5 to 15) (20 notches)
	 	x - level around size (-5 to 15) (20 notches)

x - egg examine feedback
	- draw:
		x - combined stats background (300 x 80)

x - more orders	
	x - order_duality_1 - have red and blue, min total level 3
	x - order_duality_2 - have other grow and other shrinker, min total level 6
	x - order_duality_3 - have basic and anti basic, min total level 10
	
	x - order_size_challenge_1 - 6 maximum
	x - order_size_challenge_2 - 9 minimum
	
	x - order_parts_challenge_1 - have basic grow, other grow, shrink, delayed grow, red and blue	
	x - order_parts_challenge_2 - have all ten parts

x - have pre-reqs for orders submitted before adding more (ditch the nonsensical 3 orders at a time thing)

x - fix bug where qualifying egg gets pity score

COSMETIC TWEAKS:

x - give alien the antennae
- more alien animation/frames
	- head animation looking at things moused over
		x- facing left
		x - facing right
		x - facing slight down and left
		x - facing slight down and right
		x - looking at body, claws on hips

x - change color of orders in help documentation to actually reflect final graphics

x - make chute darker

x - fix the internals mask to not include the outline

x - add arms behind inspected item (700x700 circle, image itself should probably be 1000x700)



v 0.3:

- order examine feedback
	- draw:
		- dot clusters 0 to 10 (64x64)
		- less than or equal to sign (64x64)
		- greater than or equal to sign (64x64)
	- desired total level
		- do level icon: dot cluster
	- desired size
		- do dot cluster (0 default) <= size icon <= dot cluster (10 default)
	- desired parts (reuse symbols)


- have chute give feedback as to what egg is missing, but allow sending anyway
	- graphic that rotates up a screen behind it
		- draw:
			- screen on arm (150x100)
			- icons for screen (60x60)
				- total size
					- off
					- good (green)
					- bad (red X over it)
				- total level
					- off
					- good (green)
					- bad (red X over it)
				- parts
					- off
					- good (green)
					- bad (red X over it)
	- shows up to three icons indicating issue:
		- total size
		- total level
		- parts insufficient

- drifting title screen graphics

- eggs should be put in like a holding pallet

- animate objects going into the submission chute
- animate orders coming out of the dispensers
	- when being spawned
	- when being put back

- make some tools:
	- client order editor:
		- button to automatically find and populate images based on label
		- allow specifying pre-reqs in a nice simple way
		- allow specifying required parts in a nice simple way
	- grow part editor:
		- button to automatically find and populate images based on label
		- set up relationships to other parts in a nice simple way		

- intro graphics
	- alien waking up from sleep
	- yawn
	- traipse down corridor
	- get into work
	- orders being sent up from dispenser
- outro graphics
	- alien walking out of door
	- alien walking back into room
	- alien eating something
	- document dispensed
	- alien picks it up

- ending:
	- grade based on score
	- get a report from higherups with alien reading it and appropriate facial reaction

v 0.4:

- animated alien
	- skeletal animation for alien in main room
		- idle bounce
		- idle scratch
		- idle tail flick
	- skeletal animation for alien in grow
		- moving head
		- moving arms
	- sweat when at max capacity in grow

- animated tubes


---





---

credits:

fonts
Slapface - ChevyRay

music
High Funktioning AUTISM - Spring
Plomo - 3uhox
Funked Up - Joth
Honey Bear - TinyWorlds

sound

marbles - 15cm ceramic bowl - drop - 1 ~13mm marble 07.wav - Anthousai
Wet Splat 2.mp3 - nebulasnails
wet slop plop.wav - Eneasz
Bass Kick.wav - venki88
Drink / Drinking liquid - Breviceps
viscous liquid fall in.aiff - bevibeldesign
Water Gloops & Glops.wav - joe_anderson22
Hover 1 - plasterbrain
egg crack sounds mixed.wav - Reitanna
splat2.ogg - gprosser
splat.ogg - gprosser
Slime 28.wav - Archos
Slimy Pasta Stir.wav - duckduckpony
Squelch 2 - magnuswaker
Bubbling2.wav - romulofs

----


successful all run

other_grower
other_shrinker
decayer
shrinker
anti_basic
anti_shrinker
basic_grower
delayed_grower
affector_red
affector_blue
level 22


todo:

x - background for grow scene
x - background for work scene
x - background for inspect scene
x - clipping background for grow scene


- observation scene that allows:
	x - examine order
	x - examine document
	- examine egg (use level and size and most advanced part to determine appearance?)
		- reveal egg parts size and level in thought bubble
		- need icons to convey:
			- size
			- level
	x - back button
	

	- graphical updates: 
		x - back arrow button
		
		- size indicator icon
		- level indicator icon
		- numeric marks
			- tallies from 0 to 30 to use as progress bars
		- use organ icons




		- lowest priority
			- animated claws + arms on object
			- move object itself about, parallax based on mouse cursor
		- background

	- hook up graphics
	- go work on creature graphics for grow scene now

- main work scene that allows: 
	x - picking up orders and examining them
	x - picking up "help cards" and examining them
	x - moving eggs to orders and submitting them
	x - initiating egg-making

	x - add helper documents

	X - add holding area that can accept an egg and an order
	x - pick up egg and put it on holding area
	x - pick up order (already done) and put in holding area
	x - submit egg for order when both are in holding area

	x - restrict to only being able to hold one item at a time
	x - add button to examine currently held order/document/egg

	x - add button to finish the day and end the game

	graphics:
		- egg (define as a base and a pattern, size will be based on size, colours based on two highest level parts)
			x - level 1-3
				x - examine graphic (big) 700x700
				x - in-world 100x100 (button) + highlighted + clicked
			x - level 4-6
				x - examine graphic (big)
				x - in-world (button) + highlighted + clicked				
			x - level 7-10
				x - examine graphic (big)
				x - in-world (button) + highlighted + clicked				
			x - level 11+
				x - examine graphic (big)
				x - in-world (button) + highlighted + clicked
		
		help docs: lavender
		work orders: grey with three colour stripe to differentiate them

		x - explain that things go into creature to make egg document
			x - side on view 200 x 20 + highlighted + clicked
			x - preview icon 100x100 (button) + highlighted + clicked  
			x - full display 700x700
		x - explain that creature makes egg for orders document
			x - side on view
			x - preview icon (button) 
			x - full display
		x - explain that order and egg do into completion chute
			x - side on view
			x - preview icon (button) 
			x - full display
		x - explain that door is exit
			x - side on view
			x - preview icon (button) 
			x - full display
		x - tut min level
			x - side on view
			x - preview icon (button) 
			x - full display
		x - tut needs part
			x - side on view
			x - preview icon (button) 
			x - full display
		
		- background (low priority)
			- creature just chilling on some sort of squishy furniture
		x - submission chute 300x300
			x - add button 80x60
			x - clear button 80x60
			x - submit button 80x60
		x - order dispenser	(theme: grey) 230x350	
		x - document dispenser (theme: lavender) 230x350
		x - exit orifice (also the end the game button) 160x300

	- hook up graphics
	- go work on inspection scene now


- grow game that has:
	x - buttons to allow adding parts
	x - button to allow laying egg and returning to the scene
	- graphical updates: parts need graphics
		x - affector blue #27467c 	0.152, 0.274, 0.486
			x - organ 64x64
			x - biotemplate (button)
			x - symbol (for egg visualization) 32x32
		x - affector red #7e282e 	0.494, 0.156, 0.180
			x - organ 64x64
			x - biotemplate (button)
			x - symbol (for egg visualization) 32x32
		x - basic grower #218b25	0.129, 0.545, 0.145
			x - organ 64x64
			x - biotemplate (button)
			x - symbol (for egg visualization) 32x32
		x - delayed grower #af39a8	0.686, 0.223, 0.658
			x - organ 64x64
			x - biotemplate (button)
			x - symbol (for egg visualization) 32x32
		x - other grower #c9c9c9	0.788, 0.788, 0.788
			x - organ 64x64
			x - biotemplate (button)
			x - symbol (for egg visualization) 32x32
		x - shrinker #582a93	0.345, 0.164, 0.576
			x - organ 64x64
			x - biotemplate (button)
			x - symbol (for egg visualization) 32x32
		x - anti basic #2b777a	0.168, 0.466, 0.478
			x - organ 64x64
			x - biotemplate (button)
			x - symbol (for egg visualization) 32x32
		x - anti shrinker #a3a231	0.639, 0.635, 0.192
			x - organ 64x64
			x - biotemplate (button)
			x - symbol (for egg visualization) 32x32
		x - decayer #a36f2a	0.639, 0.435, 0.164
			x - organ 64x64
			x - biotemplate (button)
			x - symbol (for egg visualization) 32x32
		x - other shrinker #3e3e3e	0.243, 0.243, 0.243
			x - organ 64x64
			x - biotemplate (button)
			x - symbol (for egg visualization) 32x32
	x - graphical updates: egg button
		x - hover
		x - disabled
		x - pressed
	x - graphical updates: rotary size limit indicator
		x - background
		x - dial part
		x - foreground cover
	- low priority: make indicator shake if >10 	
	
	- go work on work scene graphics now

	- graphical update: show order requirements
		- size indicator icon
		- level indicator icon
		- numeric marks
			- tallies from 0 to 30 to use as progress bars
		- use organ icons
	

	- graphical updates: add creature (and any animation)
		- COLOURS: GREEN BODY, TURQUOISE SECONDARY, DARK AZURE INNARDS, 
		- cross-section background	
			- ALSO SET UP AS A LIGHT2D TO ACT AS A MASK

		- head
			- curious expression
			- thinking expression
			- worried expression
		- rest of body as a static image
	
	graphical updates: add background (low priority)
		- COLOURS: DARK GREEN

	- go make the title screen and ending screen

	- graphical updates: particle systems
		- sweat for concerned creature
		- drippy goop in background?	
	- inset panel:
		- creature lay egg
		- creature lay bigger egg (size >9)		

	LOWEST PRIORITY
	- graphical updates: get the linking tubes working		
		- twitchy curves - make the control points rigidbodies with twitching logic on a different collision layer! make a line a scene!!
		- animated bulges when activating other parts
		- texture for tubes
		- have offset in growable that can be adjusted by data to link to instead of direct centre

- title/ending screen
	- o v o as squishy pulsing letters twitching across the screen
	- start game button can be an egg
	- background can be other twitchy organic shapes
	- maybe a copyright notice - (c) 2021 Cirrial
	- version number of v 0.1
	- show score if there is one



if i get to sounds
	- some light squelches for confirm click
	- pick up egg sound
	- put egg down sound
	- pick up document sound
	- put document down sound
	- lay egg sound
	- dispose egg
	- submit chute 
	- some clinkies for hover


- little animated intro (time permitting) to show main character waking up and rifling through reports
	- probably an inset thing with comic panels style things instead of animated





orders:
- want specific part
- want minimum level total
- may want specific size minimum or maximum

should probably have
2 simple orders of "have this part" and "get to this total level"
3 orders of "have this part and get to this total level"
1 order of "be under this size" (something like 6)
1 order of "this size minimum" (9 would be particularly challenging)
1 order of "have allll these parts" and this is the thing i've worked out already


mechanics:

x initial size
x min size (default 1)
x delay before activation
x size increase per round
x other parts increased per round and amount (overrides maximums/minimums)


some extra parts that use pre-existing mechanics:
x - start at 3, delay 2, shrink by 1 and go down 1 level
x - start at 2, shrink basic grower down by 1 each turn
x - start at 1, decrease all others by 1 and also 1 level 
x - start at 1, increase shrinker by 1 (thus undoing its progress)






























---




not doing a puzzle platformer because that adds unnecessary overhead, instead doing some sort of
FTL style thing of moving between nodes
accumulating resources and creating creatures as a defined creator organism
(still able to apply boosting effects of biotemplates on yourself)

(this will save a bunch of time on things like platformer AI and level designs and frees up just encounter design)

navigating the rooms of an abandoned lab to get to AI server nodes to knock them out and clear the lab for use by the creators

selecting creatures and giving direct orders for them to target specific elements of the robot enemies
robot enemies will also be targeting specific organs

basic creature layout:

- eyes (affects accuracy)
- arms (affects ability to melee attack)
- legs (affects ability to move)
- heart (allows regen, vital organ)
- brain (allows action, vital organ)

internal organs can also be damaged/disabled by toxin weapons or lucky shots

if all internal organs of a creature are disabled the creature is itself disabled and must either be left behind or healed in the room
if a vital organ of a creature are destroyed, it's now dead and salvageable resources (50% of biotemplates can be salvaged with a working digester)


basic glands to test with:
- mineral gland

for creator:
- digester
- incubator

basic robot layout:

- cube chassis
- brain chip: simple (target random creature, and random system)
- sensors (affects accuracy)
- lasers (affects ranged attack)

robot weaponry:
- lasers
- nerve disruptor
- toxin darts
- gas clouds
	- sleep gas
	- toxic gas

creature weaponry:
- claws (from gland)
- toxic spit (from gland)
- emp burst (from gland)

main loop:

1. enter room with creatures
2. event, if appropriate
3. fight off machine threats in the room, if appropriate
4. loot room, or rest (possibility of more biotemplates + food vs healing up at cost of food)
5. create new creatures as desired
6. pick new room

repeat until reaching AI server nodes, which are heavily guarded

combat loop:

1. for each creature, 
	- (if a creature's brain is disabled it doesn't do anything)
	- choose its attack (if only one option don't prompt for selection)
	- choose its target
	- choose which system of the target to attack if can sense it (with success probabilities shown with relevant glands?)
		- some systems will be contained inside others and need special weapons to reach
	- attack! if it hits, damage the component or disable it based on weapon effects
2. robots do the same for the creatures with AI based on brain chip
	- (if a robot's brain chip is disabled or destroyed it doesn't do anything)
	- choose its attack and target and system based on its brain chip
	- attack
repeat until one side is down
hitpoints should be real small, like maybe 5 hp for a really robust component, assume most attacks will do 2 damage

create loop:

1. show available biotemplates
2. pick a biotemplate
3. apply biotemplate
4. apply growth to other glands/organs
5. allow completion of organism if viable (has at least three biotemplates)



























---

- releasing a bunch of synthetic-biology creatures into a laboratory to reclaim it from hostile machines


- use grow mechanics to assemble new synthetic-biology critters
- each choice is an organ/system that affect how future additions grow/develop
- create a variety of things to overcome various challenges
- 

- solid flat colour aesthetic
- i don't wanna say "candy gore" but lots of brightly coloured systems/organs/fluids


- enemies are soulless, heartless machines
- make nothing about them recognisable, make them all very angular and also haphazardly thrown together
	- evolved spaceship aesthetic


robot parts:

- chassis
	- box
	- octahedron
	- triangle
- weapon
	- flechettes
	- laser
	- self-destruct
- locomotion	
	- wheels
	- hover unit
	- none (tripod)


challenges:
- jump height
- gliding
- resistance to lasers

creature appearances:
- main color
- secondary color
- detail color

all creatures will have two arms and two digitigrade legs in a hunched raptory-position because that'll be easier to animate

have creatures you're not controlling follow along (and attack enemies if they come in range)

organs/biotemplates:

- cling gland
EFFECT: allows wall movement
LEVEL:
BOOST:
APPEARANCE: spiky legs

- jump gland
EFFECT: allows good jumps
LEVEL:
BOOST:
APPEARANCE: tail rings (thing vintage sci fi ray gun)

- boost gland
EFFECT: faster movement
LEVEL:
BOOST:
APPEARANCE: swooshy fins

- mineral gland
EFFECT: add claws
LEVEL:
BOOST:
APPEARANCE: arms end in sharp claws

- regen heart
EFFECT: regenerate health
LEVEL:
BOOST: +1 level to each gland 
APPEARANCE: glowy chest thing

- digester
EFFECT: allow consuming materials for biomass to assemble bio templates
LEVEL:
BOOST: +1 level to each 
APPEARANCE: chompier mouth

- incubator
EFFECT: allow assembling bio templates into a new creature
LEVEL: +1 bio template for next creature
BOOST: none
APPEARANCE: slightly thicker body



